<?php
declare(strict_types=1);

namespace Appel\Bladedown\Tests;

use Appel\Bladedown\BladedownServiceProvider;
use Orchestra\Testbench\TestCase as TestCaseBase;

class TestCase extends TestCaseBase
{

    public function setUp(): void
    {
        parent::setUp();

    }

    protected function getPackageProviders($app)
    {
        return [
            BladedownServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetup($app)
    {
        //
    }
}
