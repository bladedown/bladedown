<?php
declare(strict_types=1);

namespace Appel\Bladedown\Tests\Unit;

use Appel\Bladedown\Drivers\ParsedownDriver;
use Appel\Bladedown\Tests\TestCase;

class ParsedownDriverTest extends TestCase
{
    /** @test */
    public function it_transforms_markdown_into_html()
    {
        $parser = new ParsedownDriver([]);

        $html = $parser->text("# Hello");
        $this->assertEquals("<h1>Hello</h1>", $html);
    }

    /** @test */
    function it_can_transform_inline_markdown_to_html()
    {
        $parser = new ParsedownDriver([]);

        $html = $parser->line("**Hello**");
        $this->assertEquals("<strong>Hello</strong>", $html);
    }

    /** @test */
    function it_returns_an_empty_string_when_parsing_an_empty_string()
    {
        $parser = new ParsedownDriver([]);

        $this->assertEquals('', $parser->text(''));
    }
}
