# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.1 - 2020-06-23 00:45
### Updated
- `composer.json` got more useful information for _packagist_

## 1.0.0 - 2020-06-22 16:00
### Added
- Created project based upon `andreasindal/laravel-markdown` 