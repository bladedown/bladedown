# Bladedown - Markdown parser for Blade templates

A small and lightweight **Laravel 7** package for parsing markdown inside of Blade templates.

## Installation

Install it with Composer:

```sh
composer require marcandreappel/bladedown
```

Run the `php artisan vendor:publish` command to publish the configuration file.

## Usage

### Blade directive

The markdown parser can be used in Blade templates by using the `@markdown` directive:

```html
<article>
    <h1>{{ $post->title }}</h1>

    <section class="content">
        @markdown($post->body)
    </section>
</article>
```

A block-style syntax is also available:

```markdown
@markdown
# Hello world

This *text* will be **parsed** to [HTML](http://laravel.com).
@endmarkdown
```

### Facade

```php
$markdown = "# Hello";

$html = Markdown::parse($markdown); // <h1>Hello</h1>
```

### Helpers

```php
$html = markdown('# Hello'); // <h1>Hello</h1>
```

```php
$html = markdown_capture(function () {
    echo "# Hello";
    echo "\n\n";
    echo "So **cool**!";
});

// <h1>Hello</h1>
// <p>So <b>cool</b>!</p>
```

You can also resolve the parser from the service container:

```php
$parser = app('Appel\Bladedown\Parser');
$html = $parser->parse('# Hello'); // <h1>Hello</h1>

```

### Drivers

Bladedown allows you to add custom markdown drivers. In order to use a custom markdown driver, you need to create a class which implements the `Appel\Bladedown\Drivers\MarkdownDriver` interface.
The interface contains two methods: `text` and `line`. `text` is used to convert a block of markdown to HTML, while `line` is used to convert a single line.

Bladedown ships with a `ParsedownDriver` using the [Parsedown library](http://parsedown.org/) by @erusev.

## Important notes

This package is incompatible with `andreasindal/laravel-markdown`, because it is build upon it.

### Credits

* Andreas Indal [(@andreasindal)](https://github.com/andreasindal)
* Mohamed Said [(@themsaid)](https://github.com/themsaid)
* Emanuil Rusev [(@erusev)](https://github.com/erusev)

## License

See the [LICENSE](LICENSE.md) file.
