# Contributing to Bladedown

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it is:

- reporting a bug
- discussing the current state of the code
- submitting a fix
- proposing new features
- becoming a maintainer

## We Develop with Gitlab
We use Gitlab to host code, to track issues and feature requests, as well as accept pull requests.

## Ironically we use [Github Flow](https://guides.github.com/introduction/flow/index.html)

Pull requests are the best way to propose changes to the codebase. We actively welcome your pull requests through the following steps:

1. fork the repo and create your branch from `production`
2. If you add code that should be tested, add tests
3. If you change APIs, update the documentation
4. ensure the test suite passes
5. make sure your code lints
6. issue your pull request

## Any contributions you make will be under the MIT License

Your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that is a concern.

## Report bugs using Gitlabs [issues](https://gitlab.com/bladedown/bladedown/-/issues)

We use Gitlab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/bladedown/bladedown/-/issues/new).

## Write bug reports with detail, background, and sample code

**Good Bug Reports** have:

- a quick summary and/or background
- steps to reproduce the issue:
  - be precise
  - give sample code whenever possible
- tell what you expected to happen
- tell what actually did happen
- final notes (possibly including why you think this might be happening, or what you tried that did not work)

Developer *love* thorough bug reports.

## Use a Consistent Coding Style

Every commit runs through the **ecs** and **larastan** pipelines, so make shure those run smoothly locally.
