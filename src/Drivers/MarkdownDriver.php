<?php
declare(strict_types=1);

namespace Appel\Bladedown\Drivers;

interface MarkdownDriver
{
    /**
     * Parses a markdown string to HTML.
     * 
     * @param  string  $text
     * @return string
     */
    function text(string $text): string;

    /**
     * Parses a single line of markdown to HTML.
     *
     * @param  string  $text
     * @return string
     */
    function line(string $text): string;
}