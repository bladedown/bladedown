<?php
declare(strict_types=1);

namespace Appel\Bladedown\Drivers;

use Parsedown;

class ParsedownDriver implements MarkdownDriver
{
    /** @var Parsedown $parser */
    protected Parsedown $parser;

    /**
     * ParsedownDriver constructor.
     *
     * @param  array  $config
     */
    public function __construct(array $config)
    {
        $this->parser = new Parsedown;
        $this->setOptions($config);
    }

    /**
     * {@inheritDoc}
     */
    public function text(string $text): string
    {
        return $this->parser->text($text);
    }

    /**
     * {@inheritDoc}
     */
    public function line(string $text): string
    {
        return $this->parser->line($text);
    }

    /**
     * @param  array  $config
     */
    private function setOptions(array $config): void
    {
        if (isset($config['urls'])) {
            $this->parser->setUrlsLinked($config['urls']);
        }

        if (isset($config['escape_markup'])) {
            $this->parser->setMarkupEscaped($config['escape_markup']);
        }

        if (isset($config['breaks'])) {
            $this->parser->setBreaksEnabled($config['breaks']);
        }
    }

}
