<?php
declare(strict_types=1);

namespace Appel\Bladedown;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Appel\Bladedown\Drivers\ParsedownDriver;

class BladedownServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the Markdown Blade directives and publish
     * the config file.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->publishes(
            [
                __DIR__.'/../config/bladedown.php' => config_path('bladedown.php'),
            ]
        );

        Blade::directive(
            'markdown',
            function ($markdown) {
                if ($markdown) {
                    return "<?php echo app('Appel\Bladedown\Parser')->parse($markdown); ?>";
                }

                return "<?php app('Appel\Bladedown\Parser')->begin() ?>";
            }
        );

        Blade::directive(
            'endmarkdown',
            function () {
                return "<?php echo app('Appel\Bladedown\Parser')->end() ?>";
            }
        );
    }

    /**
     * Bind the Markdown facade and the parser class to
     * the container.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(
            Parser::class,
            function ($app) {
                return new Parser(new ParsedownDriver(config('bladedown') ?? []));
            }
        );

        $this->app->bind('markdown', Parser::class);
    }

}
