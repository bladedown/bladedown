<?php
declare(strict_types=1);

namespace Appel\Bladedown;

use Appel\Bladedown\Drivers\MarkdownDriver;
use Appel\Bladedown\Exceptions\InvalidTagException;

class Parser
{

    /** @var bool $capturing Indicates if the parser is currently capturing input. */
    protected bool $capturing = false;

    /** @var MarkdownDriver $driver */
    protected MarkdownDriver $driver;

    /**
     * Parser constructor.
     *
     * @param  MarkdownDriver  $driver
     */
    public function __construct(MarkdownDriver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Parses a markdown string to HTML.
     *
     * @param  string  $text
     * @return string
     */
    public function parse(string $text): string
    {
        if (empty($text)) {
            return '';
        }

        return $this->driver->text(
            static::removeLeadingWhitespace((string) $this->escape($text))
        );
    }

    /**
     * Parses a single line of markdown to HTML.
     *
     * @param  string  $text
     *
     * @return string
     */
    public function line(string $text): string
    {
        return $this->driver->line((string) $this->escape($text));
    }

    /**
     * Escape any XSS attempts related to injecting JavaScript in anchor tags.
     * Will only escape the string if the escape option is set to true in the
     * config.
     *
     * @param  string  $text
     *
     * @return string
     */
    public function escape(string $text): string
    {
        if (config('bladedown.xss', true)) {
            return (string) preg_replace('/(\[.*\])\(javascript:.*\)/', '$1(#)', $text);
        }

        return $text;
    }

    /**
     * Start capturing output to be parsed.
     *
     * @return void
     */
    public function begin(): void
    {
        $this->capturing = true;

        ob_start();
    }

    /**
     * Stop capturing output, parse the string from markdown to HTML and return
     * it. Throws an exception if outpout capturing hasn't been started yet.
     *
     * @return string
     * @throws InvalidTagException
     */
    public function end(): string
    {
        if ($this->capturing === false) {
            throw new InvalidTagException(
                "Markdown capturing have not been started."
            );
        }
        $this->capturing = false;

        $text = ob_get_clean();
        if ($text === false) {
            throw new InvalidTagException(
                "Markdown capturing broke somewhere."
            );
        }

        return $this->parse($text);
    }

    /**
     * @param  MarkdownDriver  $driver
     */
    public function setDriver(MarkdownDriver $driver): void
    {
        $this->driver = $driver;
    }

    /**
     * Removes indentation according to the indentation of the first line, of
     * the given markdown text. This prevents markdown from being rendered
     * as code in unwanted places.
     *
     * @param  string  $text
     *
     * @return string
     */
    public static function removeLeadingWhitespace(string $text): string
    {
        $i = 0;

        while ( ! $firstLine = explode("\n", $text)[$i]) {
            $i++;
        }

        preg_match('/^( *)/', $firstLine, $matches);

        return (string) preg_replace('/^[ ]{'.strlen($matches[1]).'}/m', '', $text);
    }
}