<?php
declare(strict_types=1);

if ( ! function_exists('markdown')) {
    /**
     * Short-hand helper function to parse a
     * markdown string to HTML.
     * 
     * @see \Appel\Bladedown\Parser::parse
     * @param  string  $markdown
     * @return string
     */
    function markdown($markdown)
    {
        return app('Appel\Bladedown\Parser')->parse($markdown);
    }
}

if ( ! function_exists('markdown_capture')) {
    /**
     * Short-hand helper function to parse
     * all output from a closure from markdown
     * to HTML.
     * 
     * @see \Appel\Bladedown\Parser::begin
     * @see \Appel\Bladedown\Parser::end
     * @param  Closure  $callback
     * @return string
     */
    function markdown_capture(Closure $callback)
    {
        $parser = app('Appel\Bladedown\Parser');
        $parser->begin();
        $callback();
        return $parser->end();
    }
}
