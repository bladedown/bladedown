<?php
declare(strict_types=1);

namespace Appel\Bladedown\Exceptions;

use Exception;

class InvalidTagException extends Exception {}
